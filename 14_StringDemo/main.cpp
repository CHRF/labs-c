#include <iostream>

#include "MyStr.h"

using namespace std;

int main()
{


	MyStr a("Hello, ");
	MyStr b("World!");

	a.printStr();
	cout << endl;

	cout << "Hello, + World! = ";
	MyStr c = a + b;
	c.printStr();
	cout << endl;

	cout << "Hello, ^ 3 = ";
	MyStr d = a ^ 3;
	d.printStr();
	cout << endl;

	cout << "Hello, Hello, Hello, += World! : ";
	d += b;
	d.printStr();
	cout << endl;

	cout << "MyStr 'Hello, Hello, Hello,' = 'Goodbye' : ";
	d = "Goodbye";
	d.printStr();
	cout << endl;

	cout << "Goodbye' + 'cruel world...' : ";
	cout << d + " cruel world..." << endl;


	cout << "Hello, == World! : ";
	cout << (a == b? "true" : "false") << endl;

	cout << "Hello, == Hello, : ";
	cout << (a == a ? "true" : "false") << endl;

	cout << "Hello, != Hello, : ";
	cout << (a != a ? "true" : "false") << endl;

	cout << "first char from Hello, : ";
	cout << a[0] << endl;

	cout << "reloaded << : ";
	cout << a << endl;

	cout << "reloaded >> : ";
	MyStr f;
	cin >> f;
	cout << f << endl;

	cout << "found " << a.searchSym('l') << " 'l' in " << a << endl;
	cout << "found " << a.searchWord("ell") << " 'ell' in " << a << endl;

	return 0;
}
