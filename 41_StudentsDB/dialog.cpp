#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog),
    index(0)
{
    ui->setupUi(this);
    label = new QLabel;
    for (int i = 0; i < N; i++)
        img_list.append(QImage(":/" + QString::number(i) +
                          ".jpg"));
    img_cur = img_list[index].scaled(150, 200);
    label->setPixmap(QPixmap::fromImage(img_cur));

    up = new QPushButton("UP");
    down = new QPushButton("DOWN");

    text = new QTextEdit;
    readMyXML();
    text->append("<b>Id: </b>" + data[0].id);
    text->append("<b>Name: </b>" + data[0].name);
    text->append("<b>Age: </b>" + data[0].age);
    text->append("<b>Job: </b>" + data[0].job);
    text->setReadOnly(true);

    hlay = new QHBoxLayout;
    vlay = new QVBoxLayout;

    vlay->addWidget(label);
    vlay->addWidget(up);
    vlay->addWidget(down);

    hlay->addLayout(vlay);
    hlay->addWidget(text);

    setLayout(hlay);

    connect(up, SIGNAL(clicked()), this, SLOT(slotUp()));
    connect(down, SIGNAL(clicked()), this, SLOT(slotDown()));
}
void Dialog::readMyXML(void)
{

    QFile * file = new QFile(":/writers.xml");
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox mess;
        mess.setText("File error!");
        mess.exec();
        return;
    }
    QXmlStreamReader xml(file);
    QXmlStreamReader::TokenType token;
    Emploer temp;
    while (!xml.atEnd() && !xml.hasError())
    {
        token=xml.readNext();
        if (token == QXmlStreamReader::StartDocument)
            continue;
        if (token == QXmlStreamReader::StartElement)
        {
            if (xml.name() == "my_stud")
                continue;
            if (xml.name() == "emploer")
            {

                QXmlStreamAttributes attrs = xml.attributes();
                if (attrs.hasAttribute("id"))
                    temp.id=attrs.value("id").toString();
                continue;
            }
            if (xml.name() == "name")
            {
                xml.readNext();
                temp.name=xml.text().toString();
                continue;
            }
            if (xml.name() == "age")
            {
                xml.readNext();
                temp.age=xml.text().toString();
                continue;
            }
            if (xml.name() == "job")
            {
                xml.readNext();
                temp.job=xml.text().toString();
                continue;
            }

        }
        if (token == QXmlStreamReader::EndElement)
        {
            if (xml.name() == "emploer")
            {
                data.append(temp);
            }
        }
    }
}

void Dialog::slotUp()
{
    if (index < N -1)
    {
        index++;
        img_cur = img_list[index].scaled(150, 200);
        label->setPixmap(QPixmap::fromImage(img_cur));
        text->clear();
        text->append("<b>Id: </b>" + data[index].id);
        text->append("<b>Name: </b>" + data[index].name);
        text->append("<b>Age: </b>" + data[index].age);
        text->append("<b>Job: </b>" + data[index].job);
    }
}
void Dialog::slotDown()
{
    if (index>0)
    {
        index--;
        img_cur = img_list[index].scaled(150, 200);
        label->setPixmap(QPixmap::fromImage(img_cur));
        text->clear();
        text->append("<b>Id: </b>" + data[index].id);
        text->append("<b>Name: </b>" + data[index].name);
        text->append("<b>Age: </b>" + data[index].age);
        text->append("<b>Job: </b>" + data[index].job);
    }
}

Dialog::~Dialog()
{
    delete ui;
}