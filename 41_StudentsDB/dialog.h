#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtWidgets>
#include <QtCore>


namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    enum {N=5};
    struct Emploer
    {
        QString id;
        QString name;
        QString age;
        QString job;
    };

    QList <QImage> img_list;
    QList <Emploer> data;
    QImage img_cur;
    int index;

    QPushButton *up;
    QPushButton *down;
    QLabel * label;
    QTextEdit * text;
    Ui::Dialog *ui;
    QVBoxLayout * vlay;
    QHBoxLayout * hlay;

    void readMyXML(void);

private slots:
    void slotUp();
    void slotDown();
};

#endif // DIALOG_H