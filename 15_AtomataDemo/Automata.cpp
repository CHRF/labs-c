#include "Automata.h"
#include "Currency.h"
#include "MyStr.h"

#include <cstdlib>
#include <iostream>

using namespace std;

const int maxMenuSize = 16;

Automata::Automata()
{
	Currency cash;
	menu = new MyStr[maxMenuSize];
	menu[0]="cancel";
	menuSize = 1;
	prices = new Currency[maxMenuSize];
	status = off;

	water = new int[maxMenuSize];
	coffee = new int[maxMenuSize];
	milk = new int[maxMenuSize];
	cacao = new int[maxMenuSize];
	tea = new int[maxMenuSize];
	sugar = new int[maxMenuSize];

	waterFull = 100;
	coffeeFull = 100;
	milkFull = 100;
	cacaoFull = 100;
	teaFull = 100;
	sugarFull = 100;
}

Automata::~Automata()
{
	delete[] menu;
	delete[] prices;
}

void Automata::setON()
{
	status = on;
	cout << "ver 1.0" << endl;
	cout << "check water...\t" << "OK" << endl;
	cout << "load menu...\t" << "OK" << endl;

	loadMenu("Espresso", Currency(15, 50), 10, 10, 0, 0, 0);
	loadMenu("Cappuchino", Currency(20, 30), 10, 10, 10, 0, 0);
	loadMenu("Americano", Currency(16, 50), 20, 10, 10, 0, 0);
	loadMenu("Black Tea", Currency(13, 20), 20, 0, 0, 10, 0);
	loadMenu("Chocolate", Currency(24, 00), 0, 0, 20, 0, 10, 5);
	loadMenu("Hot Water", Currency(5, 00), 20, 0, 0, 0, 0);

	cout << "check coffee...\t" << "OK" << endl;
	cout << "check milk...\t" << "OK" << endl;

	status = wait;
	operate();
}

void Automata::operate()
{

	while (status != off)
	{
		putCoin();
	}
}

void Automata::setOFF()
{
	if (status != off)
	{
	cout << "shut down..." << endl;
	status = off;
	}
}

void Automata::loadMenu(const MyStr & _item, const Currency & _cost, const int& _water, const int& _coffee, const int& _milk, const int& _tea, const int& _cacao, const int& _sugar)
{
	if (menuSize < maxMenuSize)
	{

		menu[menuSize] = _item;
		prices[menuSize] = _cost;
		water[menuSize] = _water;
		coffee[menuSize] = _coffee;
		milk[menuSize] = _milk;
		tea[menuSize] = _tea;
		cacao[menuSize] = _cacao;
		sugar[menuSize] = _sugar;
		menuSize++;
	}

	else cout << "error: menu full!!!" << endl;
}

void Automata::printMenu()const
{
	cout << "menu:" << endl;
	for (int i = 1; i < menuSize; ++i)
	{
		cout << i << "-  " << menu[i] << "\t " << prices[i] << endl;
	}
	cout << endl;
}

void Automata::putCoin()
{
	status = accept;
	printMenu();
	cout << "\nplease add money:" << endl;
	cout << "1 - 10rub | 2 - 5rub | 3 - 1rub | 4 - 50kop | 5 - 10Kop | 0 - done | 9 - cancel" << endl  ;
	int coin;
	while (status==accept)
	{
		cout << "you have: " << cash << endl;
		cout << "you put: ";
		cin >> coin;
		if (coin == 0) { getChoice(); }
		else if (coin > 5) finish();
		else addToCash(coin);
	}
}

void Automata::addToCash(const int coin)
{
	status = check;
	Currency addCoin;
	switch (coin)
	{
		case 1: 	addCoin.setRub(10); break;
		case 2: 	addCoin.setRub(5); break;
		case 3: 	addCoin.setRub(1); break;
		case 4: 	addCoin.setKop(50); break;
		case 5: 	addCoin.setKop(10); break;
	}
	
	cash += addCoin;
	status=accept;
}



void Automata::getChoice()
{
	system("clear");
	status = accept;
	printMenu();
	cout << "you have" << cash << endl;
	cout << "please choice # (0 to cancel):";
	int menuNo;
	cin >> menuNo;
	if (menuNo <= 0 && menuNo >= menuSize) finish();

	char sugarChoice;
	cout << "maybe more sugar? it costs 1 rub. (y/n) : ";
	cin >> sugarChoice;

	choice(menuNo, sugarChoice == 'y' || sugarChoice == 'Y' ? 1 : 0);
}

void Automata::choice(const int menuNo, const bool sugarChoice)
{
	status = check;
	Currency sugarCost(sugarChoice);
	if (prices[menuNo] + sugarCost  > cash)
	{
		system("clear");
		cout << "not enough money, please add " << prices[menuNo] + sugarCost - cash << endl;
		putCoin();
	}
	else cooking(menuNo, sugarChoice);
}

void Automata::cooking(const int menuNo, const bool sugarChoice )
{
	status = cook;
	system("clear");
	cout << "please wait..." << (char)7 << (char)7 << (char)7 << endl;

	int empty = 0;
	if (water[menuNo] > waterFull)		{ cout << "error: not enough water" << endl; empty++;}
	if (coffee[menuNo] > coffeeFull)	{ cout << "error: not enough coffee" << endl; empty++; }
	if (milk[menuNo] > milkFull)		{ cout << "error: not enough milk" << endl; empty++; }
	if (tea[menuNo] > teaFull)			{ cout << "error: not enough tea" << endl; empty++; }
	if (cacao[menuNo] > cacaoFull)		{ cout << "error: not enough cacao" << endl; empty++; }
	if (sugar[menuNo] > sugarFull)		{ cout << "error: not enough sugar" << endl; empty++; }
	if (empty > 0) finish();

	waterFull -= water[menuNo];
	coffeeFull -= coffee[menuNo];
	milkFull -= milk[menuNo];
	teaFull -= tea[menuNo];
	cacaoFull -= cacao[menuNo];
	sugarFull -= sugar[menuNo]+(static_cast<int>(sugarChoice)*5);

	cout << endl;
	cout << "      (   )" << endl;
	cout << "       ) (    (" << endl;
	cout << "    .-( - ) --)-." << endl;
	cout << "   (   )     (   )" << endl;
	cout << "   \\`-.._____..-'/" << endl;
	cout << "   \\`-.._____..-'/ " << endl;
	cout << "   \\`-.._____..-'/ " << endl;
	cout << "   \\`-.._____..-'/" << endl;
	cout << "   \\`-.._____..-'/" << endl;
	cout << "   \\`-.._____..-'/   " << endl;
	cout << "    \\           /    " << endl;
	cout << "    |-.._____..-| " << endl;
	cout << "    `-.._____..-'" << endl;
	cout << (char)7 << (char)7 << (char)7 << endl;
	cout << "       Thanks!" << endl;
	cout << endl;


	cash -= prices[menuNo]+static_cast<int>(sugarChoice);
	finish();
}

void Automata::finish()
{
	status = on;
	cout << "please don't forget take your change:" << cash << endl;
	cash.setRub(0); cash.setKop(0);

	cout << "maybe drink another cup? (y/n) :";
	char getOFF;
	cin >> getOFF;

	system("clear");

	if (getOFF=='Y' || getOFF=='y' || getOFF=='1') status=wait;
	else setOFF();


}
