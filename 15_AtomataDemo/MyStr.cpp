#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include "MyStr.h"

using namespace std;

MyStr::MyStr()
{
	string = new char[256];
	string[0] = 0;
}

MyStr::MyStr(const int len)
{
	string = new char[len];
	string[0] = 0;
}

MyStr::MyStr(const char * _string)
{
	string = new char[strlen(_string) + 1];
	strcpy(string, _string);
}

MyStr::MyStr(const MyStr & _other)
{
	
	string = new char[strlen(_other.string) + 1];
	strcpy(string, _other.string);

}

MyStr::~MyStr()
{
	delete [] string;
}


MyStr operator+(const MyStr& _left,const MyStr& _right)
{
	MyStr temp(strlen(_left.string)+strlen(_right.string)+1);

	strcat(temp.string, _left.string);
	strcat(temp.string, _right.string);

	return temp;
}

MyStr MyStr::operator^(const int steps)
{
	MyStr temp((strlen(string) * steps) + 1);

	for (int i = 0; i < steps; ++i)
	strcat(temp.string, string);

	return temp;
}

MyStr & MyStr::operator=(const MyStr& _other)
{
	//���� x=x
	if(this==&_other)
		return *this;

	delete [] string;

	string = new char[strlen(_other.string) + 1];
	strcpy(string, _other.string);

	return *this;
}

MyStr & MyStr::operator+=(const MyStr& _other)
{

	*this = *this + _other;

	return *this;
}

bool operator==(const MyStr& _left,const MyStr& _right)
{
	return strcmp(_left.string, _right.string)==0;
}

bool operator!=(const MyStr& _left, const MyStr& _right)
{
	return strcmp(_left.string, _right.string)!=0;
}

char MyStr::operator[](const int _charNum)const
{
	return string[_charNum];
}

ostream & operator<<(ostream& stream, const MyStr & _outStr)
{
	stream << _outStr.getStr();
	return stream;
}

istream & operator>>(istream& stream, MyStr & _inStr)
{
	MyStr *tempStr = new MyStr(256);
	cout << "please enter string: ";
	stream >> tempStr->string;
	delete[] _inStr.string;
	_inStr.string = new char[strlen(tempStr->string) + 1];
	strncpy(_inStr.string, tempStr->string, strlen(tempStr->string) + 1);

	return stream;
}

int MyStr::searchSym(const char & ch)const
{
	int charCounter = 0;
	for (unsigned int i = 0; i <= strlen(string); ++i)
	{
		if (string[i] == ch) charCounter++;
	}

	return charCounter;
}

int MyStr::searchWord(const char * qStr)const
{
	int wordCounter = 0;
	unsigned int i, j;
	for (i = 0; i <= strlen(string); ++i)
	{
		j = 0;
		while (string[i] == qStr[j])
		{
			j++;
			if (j == strlen(qStr)) { wordCounter++; break; }
			i++;
		}

	}

	return wordCounter;
}