#ifndef MY_STR_H
#define MY_STR_H
#include <iostream>

using namespace std;


class MyStr
{
	//����� � �����
	friend ostream & operator<<(ostream& stream, const MyStr & _outStr);
	//������� �� ������
	friend istream & operator>>(istream& stream, MyStr & _inStr);
	//���������� �������� �����
	friend MyStr operator+(const MyStr& _left,const MyStr& _right);
	//���������� ���������
	friend bool operator==(const MyStr&,const MyStr&);
	friend bool operator!=(const MyStr&,const MyStr&);
public:
	//������ ������ ����� 256
	MyStr();
	//�������� ������
	MyStr(const char *);
	//������ ������ ������������ �����
	MyStr(const int);
	//����������� �����������
	MyStr(const MyStr &);
	//����������
	~MyStr();

	//������� ������
	char * getStr()const { return string; } 
	//����������� ������
	void printStr()const { cout << string; } 

	//������������ ���������
	MyStr  operator^(const int);
	MyStr& operator=(const MyStr&);
	MyStr& operator+=(const MyStr&);

	char operator[](const int)const;

	//����� �������
	int searchSym(const char &)const;
	//����� ������
	int searchWord(const char *)const;

private:
	char * string;

};



#endif