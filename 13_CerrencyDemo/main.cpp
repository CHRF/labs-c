#include <iostream>
#include "Currency.h"

using namespace std;

int main()
{
	Currency sum0;
	Currency sum1(10,20);
	Currency sum2(5, 10);

	cout << "sum1: ";
	sum1.printCurrency();
	cout << endl;
	cout << "sum2: ";
	sum2.printCurrency();
	cout << endl;

	cout << "operator+  : ";
	sum0 = sum1 + sum2;
	sum0.printCurrency();
	cout << endl;

	cout << "operator-  : ";
	sum0 = sum1 - sum2;
	sum0.printCurrency();
	cout << endl;

	cout << "operator+= : ";
	sum0 += sum2;
	sum0.printCurrency();
	cout << endl;

	cout << "operator== : ";
	cout << (sum1 == sum2) << endl;

	cout << "exchange USD : ";
	cout << sum1.GetUSD() << " USD" << endl;

	cout << "exchange EUR : ";
	cout << sum1.GetEUR() << " EUR" << endl;

	cout << "operator<< : ";
	cout << sum1  << endl;

	cout << "operator>> : ";
	Currency sum3;
	cin >> sum3;
	cout << sum3 << endl;

	return 0;
}
