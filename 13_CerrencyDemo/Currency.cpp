#include <iostream>
#include "Currency.h"
using namespace std;

static Currency exchUSD(36, 40);
static Currency exchEUR(46, 40);

Currency::Currency()
{
	rub = 0;
	kop = 0;
}


Currency::Currency(const int _rub, const int _kop)
{
	rub = _rub >= 0 ? _rub : -_rub;
	if (_kop >= 100) 
        { 
           rub += _kop / 100; 
           kop = _kop % 100; 
        }
	else 
           kop = _kop >= 0 ? _kop : -_kop;
}

Currency::Currency(const Currency & _other)
{
	rub = _other.rub;
	kop = _other.kop;
}

void Currency::setKop(int _kop)
{
	if (_kop >= 100) 
        {  
           rub += _kop / 100;  
           kop = _kop % 100; 
        }
	else 
           kop = _kop >= 0 ? _kop : -_kop;
} 

void Currency::printCurrency()const
{
	cout << getRub() << " r. " << getKop() << " k. ";
}



Currency operator+(const Currency & _left, const Currency & _right)
{
	Currency temp;

	temp.rub = _left.rub + _right.rub;
	temp.kop = _left.kop + _right.kop;
	if (temp.kop >= 100) 
        { 
           temp.rub += temp.kop / 100; 
           temp.kop = temp.kop % 100; 
        }

	return temp;
}

Currency operator-(const Currency & _left, const Currency & _right)
{
	Currency temp;

	temp.rub = _left.rub - _right.rub;
	if (_left.kop>=_right.kop)
	temp.kop = _left.kop - _right.kop;
	else
	{
		temp.rub -= 1;
		temp.kop = _left.kop + 100 - _right.kop;
	}
	if (temp.kop >= 100) 
        { 
           temp.rub += temp.kop / 100; 
           temp.kop = temp.kop % 100; 
        }
	return temp;
}

Currency& Currency::operator=(const Currency & _other)
{
	if(this==&_other) return *this;
	rub = _other.rub;
	kop = _other.kop;

	return *this;
}

Currency& Currency::operator+=(const Currency & _other)
{
	rub += _other.rub;
	kop += _other.kop;
	if (kop >= 100) 
        { 
           rub += kop / 100; 
           kop = kop % 100; 
        }
	return *this;
}

Currency& Currency::operator-=(const Currency & _other)
{
	rub -= _other.rub;
	if (kop>=_other.kop)
		kop -= _other.kop;
	else
	{
		rub -= 1;
		kop = kop + 100 - _other.kop;
	}
	if (kop >= 100) 
        { 
           rub += kop / 100; 
           kop = kop % 100; 
        }
	return *this;
}

bool operator==(const Currency & _left, const Currency & _right)
{
	return _left.rub == _right.rub && _left.kop == _right.kop ? 1 : 0;
}

bool operator!=(const Currency & _left, const Currency & _right)
{
	return _left.rub == _right.rub && _left.kop == _right.kop ? 0 : 1;
}

bool operator<(const Currency & _left, const Currency & _right)
{
	if (_left.rub < _right.rub) return 1;
	else if (_left.rub == _right.rub &&_left.kop < _right.kop) return 1;
	else return 0;
}

bool operator>(const Currency & _left, const Currency & _right)
{
	if (_left.rub > _right.rub) return 1;
	else if (_left.rub == _right.rub &&_left.kop > _right.kop) return 1;
	else return 0;
}

bool operator<=(const Currency & _left, const Currency & _right)
{
	if (_left == _right) return 1;
	else if (_left.rub < _right.rub) return 1;
	else if (_left.rub == _right.rub &&_left.kop < _right.kop) return 1;
	else return 0;
}

bool operator>=(const Currency & _left, const Currency & _right)
{
	if (_left == _right) return 1;
	else if (_left.rub > _right.rub) return 1;
	else if (_left.rub == _right.rub &&_left.kop > _right.kop) return 1;
	else return 0;
}

double Currency::GetUSD()const
{
	double usd, rubl;

	usd = static_cast<double>(exchUSD.getRub()) + (exchUSD.getKop() / 100);
	rubl = static_cast<double>(rub) + (kop / 100);

	return rub / usd;
}

double Currency::GetEUR()const
{
	double eur, rubl;

	eur = static_cast<double>(exchEUR.getRub()) + (exchEUR.getKop() / 100);
	rubl = static_cast<double>(rub)+(kop / 100);

	return rub / eur;
}


ostream & operator<<(ostream& stream, const Currency & curr)
{

	stream << curr.getRub() << " rub. ";
	stream << curr.getKop() << " kop. ";

	return stream;
}

istream & operator>>(istream& stream, Currency & curr)
{
	int i;
	cout << "please input currency (rub, kop)" << endl;
	stream >> i;
	curr.setRub(i);
	stream >> i;
	curr.setKop(i);

	return stream;
}


void setUSD(int _rub, int _kop)
{
	exchUSD.setRub(_rub);
	exchUSD.setKop(_kop);
}

void setEUR(int _rub, int _kop)
{
	exchEUR.setRub(_rub);
	exchEUR.setKop(_kop);
}
