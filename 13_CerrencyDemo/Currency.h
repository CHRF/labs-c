#ifndef CURRENCY_H
#define CURRENCY_H
#include <iostream>

using namespace std;


class Currency
{

friend Currency operator + (const Currency & _left, const Currency & _right);
friend Currency operator - (const Currency & _left, const Currency & _right);
friend bool operator ==	(const Currency & _left, const Currency & _right);
friend bool operator !=	(const Currency & _left, const Currency & _right);
friend bool operator >	(const Currency & _left, const Currency & _right);
friend bool operator <	(const Currency & _left, const Currency & _right);
friend bool operator >=	(const Currency & _left, const Currency & _right);
friend bool operator <=	(const Currency & _left, const Currency & _right);

public:
	Currency();
	Currency(const int, const int = 0);
	Currency(const Currency &);

	int getRub()const { return rub; }
	int getKop()const { return kop; }
	void setRub(int _rub) { rub = _rub >= 0 ? _rub : -_rub; }
	void setKop(int _kop);
	void printCurrency()const;


	Currency& operator=(const Currency &);
	Currency& operator+=(const Currency &);
	Currency& operator-=(const Currency &);


	double GetUSD()const;
	double GetEUR()const;

private:
	int rub;
	int kop;

};

ostream & operator<<(ostream& stream, const Currency & curr);
istream & operator>>(istream& stream, Currency & curr);

#endif
