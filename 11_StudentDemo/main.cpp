#include <iostream>
#include <cstring>
#include "student.h"
#include "group.h"

using namespace std;


int main()
{
	student *a = new student("Fedya", "Fedorow");
	student *b = new student("Petr", "Petrov");
        student *c = new student("Vasya", "Smirnov");
	a->addGrade(5);
	a->addGrade(4);
	a->addGrade(3);
	a->addGrade(3);
	a->addGrade(4);
	b->addGrade(3);
	b->addGrade(3);
	b->addGrade(4);
        c->addGrade(1);
        c->addGrade(0);
        c->addGrade(3);
        c->addGrade(2);

	//init group
        group *A = new group ("myGroup");
	//add students 
	A->addStudent(a);
	A->addStudent(b);
        A->addStudent(c);
	//print group
	A->printGroup();
	cout << "average:" << A->avGroup() << endl;
	//delete Smirnov
	A->removeStudent(c);
	cout <<"\n after delete student" << endl;
	b->addGrade(5);
	A->printGroup();
	cout <<"average:"<< A->avGroup() << endl;
	a->printStudent();

	return 0;
}
