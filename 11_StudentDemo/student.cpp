#include <iostream>
#include <cstring>
#include "student.h"

using namespace std;


student::student()
{
	firstname = NULL;
	lastname = NULL;
	grade = NULL;
}

student::student(const char * newfirstname, const char * newlastname)
{
	firstname = new char[strlen(newfirstname)+1];
	strcpy(firstname, newfirstname);
	lastname = new char[strlen(newlastname)+1];
	strcpy(lastname, newlastname);
	grade = new int[64];
}

student::~student()
{
	delete [] firstname;
	delete [] lastname;
	delete[] grade;
}

char *student::getFirstname()const
{
	return firstname;
}

char *student::getName()const
{
	return lastname;
}

int student::getGrade(const int gradeNo)const
{
	return grade[gradeNo];
}

void student::printStudent()const
{
	cout << getFirstname() << ' ' << getName() << ": ";

	int gradeNo=0;
	while (grade[gradeNo] <= 5 && grade[gradeNo] > 0)
	{
		cout << getGrade(gradeNo) << ' ';
		gradeNo++;
	}
	cout << "average:" << avGrade() <<  endl;
}

bool student::addGrade(const int newgrade)
{
	if (newgrade <= 5 && newgrade > 0)
	{
	
		for (int i = 0; i < 64; i++)
		{
			if (grade[i] >= 5 || grade[i] < 1) 
                        { 
                            grade[i] = newgrade; 
                            return 1;
                        }
		}
		cout << "not memory for grade" << endl;

	}
	return 0;
}

double student::avGrade()const
{
	int gradeNo = 0, gradeSum=0;
	while (grade[gradeNo] <= 5 && grade[gradeNo] >= 1)
	{
		gradeSum += grade[gradeNo];
		gradeNo++;
	}
	return double (gradeSum) / gradeNo;
}
