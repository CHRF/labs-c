#ifndef STUDENT_H
#define STUDENT_H

//#include <iostream>

using namespace std;

class student
{
	friend class group;
public:
	student();
	student(char const*, char const*);
	~student();
	char * getFirstname()const;
	char * getName()const;
	void printStudent()const;
	int getGrade(const int)const;
	bool addGrade(const int);
	double avGrade()const;

private:
	char * firstname;
	char * lastname;
	int * grade;
};

#endif
