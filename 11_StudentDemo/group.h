#ifndef GROUP_H
#define GROUP_H

#include "student.h"

using namespace std;

class group
{
public:
	group(const char *);
	~group();
	void addStudent(student *);
	void removeStudent(student *);
	void printGroup()const;
	double avGroup()const;
	int getStudCount()const { return studCount; }

private:
	char *name;
	student *stud;
	int studCount;
};


#endif
