#include "mytcpsocket.h"
#include "QDate"
#include <QDebug>

MyTcpSocket::MyTcpSocket(QObject *parent) :
    QObject(parent)
{
}

void MyTcpSocket::doConnect()
{
    socket =new QTcpSocket(this);

    socket->connectToHost("localhost", 4444);


    if (socket->waitForConnected(5000))
    {
        qDebug() << "Connected!";
        socket->write("!");

        socket->waitForReadyRead(1000);
        QByteArray arr= socket->readAll();
        int h = arr[0];
        int m = arr[1];
        int s = arr[2];
        QTime t(h, m, s);
        qDebug() << t.toString();

        socket->close();
    }
    else
    {
        qDebug() << "Not connected!";
    }
}