#ifndef MYSERVER_H
#define MYSERVER_H

#include <QTcpServer>

class MyServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit MyServer(QObject *parent = 0);
    void startServer()
    {
        listen(QHostAddress::LocalHost, 4444);
        qDebug() << "Server started!";
    }

protected:
    virtual void incomingConnection(int sd);
signals:

public slots:

};

#endif // MYSERVER_H