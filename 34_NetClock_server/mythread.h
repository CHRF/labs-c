#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
#include <QTcpSocket>

class MyThread : public QThread
{
    Q_OBJECT
public:
    explicit MyThread(int sd, QObject *parent = 0);
    void run();

signals:
    void error(QTcpSocket::SocketError se);
    
public slots:
    void readyRead();
    void disconnected();
    void connected()
    {
        qDebug() << "Connected!";
    }
    
private:
    QTcpSocket * socket;

};

#endif // MYTHREAD_H