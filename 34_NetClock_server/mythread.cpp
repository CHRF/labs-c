#include "mythread.h"
#include <QtCore>
#include <QDate>
#include <QTime>

MyThread::MyThread(int sd, QObject *parent) :
    QThread(parent)
{
    qDebug() << "New connection";
    socket = new QTcpSocket;
    socket->setSocketDescriptor(sd);
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()), Qt::DirectConnection);
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);
    connect(socket, SIGNAL(connected()), this, SLOT(connected()), Qt::DirectConnection);

    
}
void MyThread::run()
{
    exec();   
}

void MyThread::readyRead()
{
    qDebug() << "read!";
    socket->readAll();
    QTime t= QTime::currentTime();
    char h = t.hour();
    char m = t.minute();
    char s = t.second();
    QByteArray arr;
    arr.append(h);
    arr.append(m);
    arr.append(s);
    socket->write(arr);
    socket->waitForBytesWritten(1000);
    socket->flush();
}

void MyThread::disconnected()
{
    qDebug() << "Disconnect";
    exit(0);
    socket->disconnectFromHost();
}