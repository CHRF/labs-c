 #include <QtGui>
 #include <QTime>

 #include "analogclock.h"

 AnalogClock::AnalogClock(QWidget *parent)
     : QWidget(parent)
 {
     QTimer *timer = new QTimer(this);
     connect(timer, SIGNAL(timeout()), this, SLOT(update()));
     timer->start(1000);

     setWindowTitle(tr("Analog Clock"));
     resize(250, 250);
 }


 void AnalogClock::paintEvent(QPaintEvent *)
 {
     static const QPoint hourHand[4] = {
         QPoint(1, 0),
         QPoint(8, -20),
         QPoint(0, -50),
         QPoint(-8, -20)
     };
     static const QPoint minuteHand[4] = {
         QPoint(1, 0),
         QPoint(8, -20),
         QPoint(0, -70),
         QPoint(-8, -20)
     };
     static const QPoint secondsHand[4] = {
         QPoint(1, 0),
         QPoint(3, -20),
         QPoint(0, -70),
         QPoint(-2, -20)
     };
     QColor hourColor(127, 0, 127);
     QColor minuteColor(0, 127, 127, 255);
     QColor secondsColor(200, 200, 100, 191);

     int side = qMin(width(), height());
     QTime time = QTime::currentTime();


     QPainter painter(this);
     painter.setRenderHint(QPainter::Antialiasing);
     painter.translate(width() / 2, height() / 2);
     painter.scale(side / 250.0, side / 250.0);

     painter.setPen(hourColor);
     painter.drawText(20, 5, QDateTime::currentDateTime().date().toString("dd.MM") );

     painter.setPen(Qt::NoPen);
     painter.setBrush(hourColor);

     painter.save();
     painter.rotate(30.0 * ((time.hour() + time.minute() / 60.0)));
     painter.drawConvexPolygon(hourHand, 4);
     painter.restore();

     painter.setPen(hourColor);


     for (int i = 0; i < 12; ++i) {

         QString roman("I");

         switch (i)
         {
                      case 0: roman="I"; break;
                      case 1: roman="II"; break;
                      case 2: roman="III"; break;
                      case 3: roman="IV"; break;
                      case 4: roman="V"; break;
                      case 5: roman="VI"; break;
                      case 6: roman="VII"; break;
                      case 7: roman="VIII"; break;
                      case 8: roman="IX"; break;
                      case 9: roman="X"; break;
                      case 10: roman="XI"; break;
                      case 11: roman="XII"; break;
         }

         painter.drawText(-76*sin((M_PI/180)*(i-5)*30)-6, 76*cos((M_PI/180)*(i-5)*30)+5, roman);

     }

     for (int i = 0; i < 12; ++i)
     {

         painter.drawLine(88, 0, 96, 0);

         painter.rotate(30.0);
     }

     painter.setPen(Qt::NoPen);
     painter.setBrush(minuteColor);

     painter.save();
     painter.rotate(6.0 * (time.minute() + time.second() / 60.0));
     painter.drawConvexPolygon(minuteHand, 4);
     painter.restore();

     painter.setPen(secondsColor);
     painter.setBrush(secondsColor);
     painter.save();
     painter.rotate(6.0 * (time.minute() + time.second()));
     painter.drawConvexPolygon(secondsHand, 4);
     painter.restore();

    painter.setBrush(minuteColor);
     painter.setPen(minuteColor);
     for (int j = 0; j < 60; ++j) {
         if ((j % 5) != 0)
         painter.drawLine(92, 0, 96, 0);
         painter.rotate(6.0);
     }

     painter.drawEllipse(-7, -7, 14, 14);


 }
