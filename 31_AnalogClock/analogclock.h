#ifndef ANALOGCLOCK_H
#define ANALOGCLOCK_H

#include <QWidget>
//#include <QtGui>

 class AnalogClock : public QWidget
 {
     Q_OBJECT

 public:
     AnalogClock(QWidget *parent = 0);

 protected:
     void paintEvent(QPaintEvent *event);
 };

 #endif
