#include "VeryLong.h"
#include <cctype>
#include <cstdlib>
#include <iostream>

using namespace std;

static string ONE = "1";
static string ZERO = "0";
static string TWO = "2";
static string MYEOF = "-1";

static void clean_str(string & s)
{
    while(s.size() && s[0] == '0')
    {
        s.erase(0, 1);
    }
    if (! s.size())
        s = "0";
}

VeryLong::VeryLong(string  s)
{
    if (s[0] == '-')
    {
        sign = false;
        s.erase(0, 1);//Delete sign from string
    }
    else
        sign = true;
    while (s.size()>1 && s[0] == '0')
        s.erase(0, 1);
    if (!s.size())
        throw "No string";
    arr = new int[s.size()];
    n = s.size();
    for (int i = 0; i < (int)s.size(); i++)
    {
        if (!isdigit(s[i]))
           throw s;
        arr[i]=s[i] - '0';
    }
}

const string VeryLong::getStr()
{
    string temp = "";
    if (sign == false)
        temp = temp + '-';
    for (int i = 0; i < n; i++)
        temp = temp + (char)(arr[i] + '0');

    return temp;
}

VeryLong::VeryLong(const VeryLong & v)
{
    n = v.n;
    sign = v.sign;
    arr = new int[n];
    for (int i = 0; i < n; i++)
        arr[i] = v.arr[i];
}

VeryLong & VeryLong::operator=(const VeryLong & v)
{
    if (this == &v)
        return *this;
    delete [] arr;
    n = v.n;
    sign = v.sign;
    arr = new int[n];
    for (int i = 0; i < n; i++)
        arr[i] = v.arr[i];

    return *this;
}

VeryLong VeryLong::operator+(VeryLong & v)
{

    string temp_str = "";
    int nextvalue=0;
    VeryLong max, min, temp_vl1, temp_vl2;
    if (sign && v.sign)
    {

        if ((*this) > v)
        {
            max = *this;
            min = v;
        }
        else
        {
            min = *this;
            max = v;
        }

        while (max.n && min.n)
        {
            max.n--;
            min.n--;
            nextvalue += max.arr[max.n] + min.arr[min.n];
            temp_str = (char)(nextvalue % 10 + '0') + temp_str;
            nextvalue = nextvalue / 10;
        }
        while (max.n)
        {
            max.n--;
            nextvalue += max.arr[max.n];
            temp_str = (char)(nextvalue % 10 + '0') + temp_str;
            nextvalue = nextvalue / 10;
        }
        if (nextvalue)
           temp_str = (char)(nextvalue + '0') + temp_str;
        clean_str(temp_str);
        return temp_str;

    }
    else if (sign == true && v.sign == false)
    {
        VeryLong temp_new = ABS(v);
        if ((*this) >= temp_new)
        {
            temp_vl1 = *this;
            int vn = v.n;
            while (vn--)
            {
                temp_vl1.n--;
                if (temp_vl1.arr[temp_vl1.n] >= v.arr[vn])
                    temp_str = (char)(temp_vl1.arr[temp_vl1.n] - v.arr[vn] + '0') + temp_str;
                else
                {
                    temp_vl1.arr[temp_vl1.n - 1]--;
                    temp_vl1.arr[temp_vl1.n] += 10;
                    temp_str = (char)(temp_vl1.arr[temp_vl1.n] - v.arr[vn] + '0') + temp_str;

                }
            }
            while (temp_vl1.n)
            {
                temp_vl1.n--;
                if (temp_vl1.n == 0 && temp_vl1.arr[0] == 0)
                    break;
                if (temp_vl1.arr[temp_vl1.n] < 0)
                {
                    temp_vl1.arr[temp_vl1.n - 1]--;
                    temp_vl1.arr[temp_vl1.n] += 10;
                }
                temp_str = (char)(temp_vl1.arr[temp_vl1.n] + '0') + temp_str;
            }
            clean_str(temp_str);
            return temp_str;
        }
        else
        {
            temp_vl1 = *this;
            !temp_vl1;
            temp_vl2 = v;
            !temp_vl2;
            temp_vl2 = temp_vl2 + temp_vl1;
            !temp_vl2;
            return temp_vl2;
        }
    }
    else if (sign == false && v.sign == true)
    {
        return v + (*this);
    }
    else
    {
        temp_vl1 = *this;
        temp_vl2 = v;
        !temp_vl1;
        !temp_vl2;
        temp_vl1 = temp_vl1 + temp_vl2;
        !temp_vl1;
        return temp_vl1;
    }
}

VeryLong operator!(VeryLong & v)
{
    if (v.getStr() != "0")
        v.sign = !v.sign;
    return v;
}

VeryLong VeryLong::operator-(VeryLong & v)
{
    VeryLong temp = v;
    !temp;
    return ((*this) + temp);
}

bool VeryLong::operator>(VeryLong & v)
{
    if (sign && v.sign)
    {
        int i;
        if (n > v.n)
            return true;
        else
        {
            for (i = 0; i < n; i++)
                if (arr[i] > v.arr[i])
                    return true;
                else if (arr[i] == v.arr[i])
                    continue;
                else
                    return false;
            return false;
        }
    }
    else if (sign == true && v.sign == false)
          return true;
    else if (sign == false && v.sign == true)
         return false;
    else
    {
        VeryLong temp1 = *this, temp2 = v;
        !temp1;
        !temp2;
        if (temp1 > temp2 && temp1 == temp2)
            return false;
        return true;
    }
}

bool VeryLong::operator==(VeryLong & v)
{
    if (n != v.n && sign != v.sign)
        return false;
    for (int i = 0; i < n; i++)
        if (arr[i] != v.arr[i])
            return false;
    return true;
}

VeryLong VeryLong::operator *(VeryLong & v)
{
    VeryLong temp = *this;
    string ONE = "1";
    string ZERO = "0";
    VeryLong one(ONE);
    VeryLong zero(ZERO);
    VeryLong times = v;
    if (times < zero)
        !times;
    if (temp < zero)
        !temp;
    VeryLong result(ZERO);
    while (times > zero)
    {
        result = result + temp;
        times = times - one;
    }
    if (sign != v.sign)
        !result;

    return result;
}

VeryLong VeryLong::operator/(VeryLong & v)
{
    VeryLong zero(ZERO);
    if (v == zero)
        throw "Can't / 0";
    VeryLong one(ONE);
    VeryLong times(ZERO);
    VeryLong temp1 = *this, temp2 = v;
    if (temp1 < zero)
        !temp1;
    if (temp2 < zero)
        !temp2;
    while ((temp1 = temp1 - temp2) >= zero)
        times = times + one;
    if (sign != v.sign)
        !times;

    return times;
}

VeryLong VeryLong::operator ^(VeryLong &v)
{
    VeryLong zero(ZERO);
    VeryLong one(ONE);
    VeryLong two(TWO);
    VeryLong result(ONE);
    VeryLong my_eof(MYEOF);
    VeryLong temp1 = *this, temp2 = v;
    if (temp1 < zero)
        !temp1;
    if (temp2 < zero)
       return my_eof;
    while (temp2 > zero)
    {
        result = result * temp1;
        temp2 = temp2 - one;
    }
    if (((*this) < zero) && ((temp1 = v % two) != zero))
    {
        !result;
    }

    return result;
}

VeryLong VeryLong::operator%(VeryLong &v)
{
    VeryLong zero(ZERO);
    if (v == zero)
        throw "Can't % 0";
    VeryLong lost = *this, temp1 = v, temp2(ZERO);
    if (lost < zero)
        !lost;
    if (temp1 < zero)
        !temp1;
    while ((temp2 = lost - temp1) >= zero)
        lost = temp2;
    if (sign != v.sign)
        !lost;

    return lost;

}
