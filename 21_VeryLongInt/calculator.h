#ifndef CALCULATOR_H
#define CALCULATOR_H
#include <QtWidgets/QWidget>
#include <QStack>
#include <QLineEdit>

class QPushButton;

class Calculator : public QWidget
{
    Q_OBJECT

private:
    QLineEdit * m_plcd;
    QStack<QString> m_stack;
    QString m_str_display;

public:
    Calculator(QWidget * parent = 0);

    QPushButton * createButton(const QString & str);
    void calculate();
public slots:
    void slotButtonClicked();
};

#endif // CALCULATOR_H
