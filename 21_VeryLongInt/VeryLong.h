#ifndef VARYLONG_H_
#define VARYLONG_H_
#include <string>
#include <QString>
using std::string;
class VeryLong
{
    int * arr;
    int n;
    bool sign;
public:
    //Basic methods
    VeryLong() { arr = NULL; n = 0; }
    VeryLong(string); //exeption: string , const char *
    VeryLong(QString s) { arr = NULL; n = 0; *this = VeryLong(s.toStdString()); }
    const string getStr();
    QString getQStr() { return QString::fromStdString(getStr()); }
    ~VeryLong(){ delete [] arr; arr = NULL; }
    VeryLong(const VeryLong &);
    VeryLong & operator=(const VeryLong &);
    //End basic methods
    //operators + - < > <= >= == != * / % ^
    VeryLong operator+(VeryLong &); //Basic operator. All ather opertors works with
                                    //help operator+.
    VeryLong operator-(VeryLong &);
    bool operator>(VeryLong &);
    bool operator==(VeryLong &);
    bool operator!=(VeryLong & v) { return (!((*this) == v)); }
    bool operator<(VeryLong & v) { return !(((*this) > v) || ((*this) == v)); }
    bool operator<=(VeryLong & v) { return (((*this) < v) || ((*this) == v)); }
    bool operator>=(VeryLong & v) { return (((*this) > v) || ((*this) == v)); }
    VeryLong operator*(VeryLong & v);
    VeryLong operator/(VeryLong & v);
    VeryLong operator%(VeryLong &v); //exeption: const char *
    VeryLong operator^(VeryLong & v);
    //Friends
    friend VeryLong operator!(VeryLong & v);
    friend VeryLong ABS(VeryLong v) { return (v.sign ? v : (!v)); }
};

#endif // HFILE_H
