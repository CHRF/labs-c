#include "calculator.h"
#include <QtWidgets/QtWidgets>
#include <QtCore>
#include "VeryLong.h"
Calculator::Calculator(QWidget *parent): QWidget(parent)
{
    m_plcd = new QLineEdit;
    m_plcd->setMinimumSize(150, 50);
    m_plcd->setText("0");

    QChar aButton [ 4][ 4] = {
        { '7', '8', '9', '/'},
        { '4', '5', '6', '*'},
        { '1', '2', '3', '-'},
        { '0', '.', '=', '+'}
    };
    QGridLayout * ptopLayout = new QGridLayout;
    ptopLayout->addWidget(m_plcd, 0, 0, 1, 4);
    ptopLayout->addWidget(createButton("CE"), 1, 3);
    ptopLayout->addWidget(createButton("%"), 1, 0);
    ptopLayout->addWidget(createButton("^"), 1, 1);

    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            ptopLayout->addWidget(createButton(aButton[ i][ j]), i + 2, j);

    setLayout(ptopLayout);
}

QPushButton * Calculator::createButton(const QString &str)
{
    QPushButton * pb = new QPushButton(str);
    pb->setMinimumSize(40, 40);
    connect(pb, SIGNAL(clicked()), SLOT(slotButtonClicked()));

    return pb;
}
void Calculator::calculate()
{
    VeryLong operand2=m_stack.pop();
    QString operation = m_stack.pop();
    VeryLong operand1=m_stack.pop();
    VeryLong result = QString("0");

    if (operation == "+")
        result = operand1 + operand2;
    if (operation == "-")
        result = operand1 - operand2;
    if (operation == "/")
        result = operand1 / operand2;
    if (operation == "*")
        result = operand1 * operand2;
    if (operation == "%")
        result = operand1 % operand2;
    if (operation == "^")
        result = operand1 ^ operand2;

    m_plcd->setText(result.getQStr());
}

void Calculator::slotButtonClicked()
{
    QString str = ((QPushButton *) sender())->text();

    if (str == "CE")
    {
        m_stack.clear();
        m_str_display = "";
        m_plcd->setText("0");

        return;
    }
    if (str.contains(QRegExp("[ 0-9]")) || str == ".")
    {
        m_str_display += str;
        m_plcd->setText(m_str_display);
    }
    else
    {
        m_stack.push(m_plcd->text());
        m_str_display="";
        if (m_stack.size() >= 2)
        {
            calculate();
            m_stack.clear();
            m_stack.push(m_plcd->text());
        }
        else
        {
            m_plcd->setText("0");
        }
        if (str != "=")
            m_stack.push(str);
        else
            m_stack.clear();
    }
}
