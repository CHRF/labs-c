#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtWidgets>
#include <QtCore>
#include <vector>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

protected:
    void paintEvent(QPaintEvent *event);

private:
    //typedef
    //form
    Ui::Dialog *ui;

    //function
    void readData();
    void setXY();
    void move_people(void);



    //data
    QVector <QString> arr;
    QImage wal;
    QImage grass;
    QImage men;
    int h, w;
    int fin_x, fin_y;
    int cur_x, cur_y;
    QTimer * timer;

};


#endif // DIALOG_H