#include "option.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{

    predator= new QLabel("Set predator");
    fish = new QLabel("Set fish");

    num_predator= new QSpinBox;
    num_predator->setMinimum(1);
    num_predator->setMaximum(10);
    num_predator->setFixedSize(45,30);
    num_fish= new QSpinBox;
    num_fish->setMinimum(10);
    num_fish->setMaximum(100);
    num_fish->setFixedSize(45,30);

    but_begin= new QPushButton("Begin");
    connect(but_begin, SIGNAL(clicked()), this, SLOT(end()));

    firs_layout= new QHBoxLayout;
    firs_layout->addWidget(predator);
    firs_layout->addWidget(num_predator);
    second_layout= new QHBoxLayout;
    second_layout->addWidget(fish);
    second_layout->addWidget(num_fish);
    layout= new QVBoxLayout;
    layout->addLayout(firs_layout);
    layout->addLayout(second_layout);
    layout->addWidget(but_begin);

    setLayout(layout);

    move(600, 100);
}
