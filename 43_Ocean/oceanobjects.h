#ifndef HFILE_H
#define HFILE_H
#include <time.h>
#include <stdlib.h>
#include <typeinfo>
//const int N = 25;
//const int M = 25;
const int SIZE_CELL = 30;
class Point
{
public:
    virtual int getLive(){}
    virtual void downLive(){}
    virtual void upLive(){}
};
class Stone : public Point
{

};
class Sea : public Point
{

};
class Fish : public Point
{
    int live;
public:
    Fish() : live(10) {}
    int getLive() { return live; }
    void downLive() { live--; }
    void upLive() { live+=2;}
};
class Predator : public Point
{
    int live;
public:
    Predator() : live(20) {}
    int getLive() { return live; }
    void downLive() { live--; }
    void upLive() { live+=3;}
};

#endif // HFILE_H
