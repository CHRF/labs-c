#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtWidgets>
#include "oceanobjects.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void setNumbers(int num_pr, int num_f)
    {
        num_predator = num_pr;
        num_fish = num_f;
     }
    void start(void)
    {
        iniMatrix();
        show();
        timer->start(2000);
    }

protected:
    void paintEvent(QPaintEvent *);
private:
    //image
    QImage i_predator;
    QImage i_fish;
    QImage i_stone;
    QImage i_sea;
    //types
    enum {N=20, M=20};
    enum {NSTONES=10};
    //functions
    void iniMatrix();
    void stepFish();
    void stepPredator();
    int getRandCoord(int, int flag);

    //data
    Ui::MainWindow *ui;
    int num_predator, num_fish;
    Point * matrix[N][M];
    QTimer * timer;

    //flag
    bool fl;
};

#endif // MAINWINDOW_H
