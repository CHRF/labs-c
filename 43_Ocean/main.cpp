#include "mainwindow.h"
#include <QApplication>
#include "option.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    Widget option;
    option.show();
    QObject::connect(&option, SIGNAL(send(int,int)), &w, SLOT(setNumbers(int,int)));
    QObject::connect(&option, SIGNAL(next()), &w, SLOT(start()));

    return a.exec();
}
