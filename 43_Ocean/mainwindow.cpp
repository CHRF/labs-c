#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdlib.h>
#include <time.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    fl = false;
    i_predator = QImage(":/predator.jpg").scaled(SIZE_CELL, SIZE_CELL);
    i_fish = QImage(":/fish.jpg").scaled(SIZE_CELL, SIZE_CELL);
    i_stone = QImage(":/stone.jpg").scaled(SIZE_CELL, SIZE_CELL);
    i_sea = QImage(":/voda.jpg").scaled(SIZE_CELL, SIZE_CELL);
    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    move(20, 20);
    resize(N * SIZE_CELL, M * SIZE_CELL +20);
    srand(time(NULL));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::iniMatrix()
{
    // ---------- create an empty sea -----------------
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            matrix[i][j] = new Sea;

    // ---------- create a stones ----------------------
    int count = NSTONES;
    while (count)
    {
        int i = rand() % N;
        int j = rand() % M;
        if (typeid(*matrix[i][j]) != typeid(Stone))
        {
            matrix[i][j] = new Stone;
            count--;
        }
    };
    // ---------- create a predators -------------------
    count = num_predator;
 //   matrix[0][0] = new Predator;
    while (count)
    {
        int i = rand() % N;
        int j = rand() % M;
        if (typeid(*matrix[i][j]) != typeid(Predator) && typeid(*matrix[i][j]) != typeid(Stone))
        {
            matrix[i][j] = new Predator;
            count--;
        }
    }

    // ---------- create a fishes ----------------------
    count = num_fish;
  //  matrix[1][1] = new Fish;
    while (count)
    {
        int i = rand() % N;
        int j = rand() % M;
        if (typeid(*matrix[i][j]) != typeid(Predator) && typeid(*matrix[i][j]) != typeid(Stone)
                && typeid(*matrix[i][j]) != typeid(Fish))
        {
            matrix[i][j] = new Fish;
            count--;
        }
    }
}
void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    if (fl)
    {
        stepFish();
        stepPredator();
    }
    else
        fl = true;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if (typeid(*matrix[i][j]) == typeid(Sea))
                painter.drawImage(i * SIZE_CELL, j * SIZE_CELL, i_sea);
            else if (typeid(*matrix[i][j]) == typeid(Stone))
                painter.drawImage(i * SIZE_CELL, j * SIZE_CELL, i_stone);
            else if (typeid(*matrix[i][j]) == typeid(Predator))
                painter.drawImage(i * SIZE_CELL, j * SIZE_CELL, i_predator);
            else if (typeid(*matrix[i][j]) == typeid(Fish))
                painter.drawImage(i * SIZE_CELL, j * SIZE_CELL, i_fish);
        }
    }
}
void MainWindow::stepFish()
{
    for(int i=0; i<N; i++)
    {
        for(int j=0;j<M; j++)
        {
            if (typeid(*matrix[i][j]) == typeid(Fish))
            {
                if (matrix[i][j]->getLive() == 0)
                {
                    delete matrix[i][j];
                    matrix[i][j] = new Sea;
                    continue;
                }
                matrix[i][j]->downLive();

                int x, y, count=0, chaildcount=0;

                for(count; count<4; count++)
                {
                    x = getRandCoord (i,0);  //flag == 0 -N-coordinate, flag == 1 - M-coordinate
                    y = getRandCoord (j,1);

                    if (typeid(*matrix[x][y]) == typeid(Fish))
                    {
                        int chaild_x, chaild_y;
                        for(chaildcount=0; chaildcount<4; chaildcount++)
                        {
                            chaild_x = getRandCoord (i,0);
                            chaild_y = getRandCoord (j,1);
                            if (typeid(*matrix[chaild_x][chaild_y]) == typeid(Sea))
                                matrix[chaild_x][chaild_y] = new Fish;
                            break;
                        }
                    }
                }
                if (chaildcount==4 && typeid(*matrix[x][y]) == typeid(Sea))
                {
                    Point * p = matrix[x][y];
                    matrix[x][y] = matrix[i][j];
                    matrix[i][j] = p;
                }

            }


        }
    }

}
void MainWindow::stepPredator()
{
    for(int i=0; i<N; i++)
    {
        for(int j=0;j<M; j++)
        {
            if (typeid(*matrix[i][j]) == typeid(Predator))
            {
                if (matrix[i][j]->getLive() == 0)
                {
                    delete matrix[i][j];
                    matrix[i][j] = new Sea;
                    continue;
                }
                matrix[i][j]->downLive();

                int x, y, count=0;
                for(count; count<4; count++)
                {
                    x = getRandCoord (i,0);  //flag == 0 -N-coordinate, flag == 1 - M-coordinate
                    y = getRandCoord (j,1);

                    if (typeid(*matrix[x][y]) == typeid(Fish)) //eat
                    {
                        matrix[i][j]->upLive();
                        delete matrix[x][y];
                        matrix[x][y] = new Sea;
                        Point * p = matrix[x][y];
                        matrix[x][y] = matrix[i][j];
                        matrix[i][j] = p;
                        break;
                    }
                }

                if (count==4 && typeid(*matrix[x][y]) == typeid(Sea)) //step
                {
                      Point * p = matrix[x][y];
                      matrix[x][y] = matrix[i][j];
                      matrix[i][j] = p;
                }


            }


        }
    }
}


int MainWindow::getRandCoord(int coordinate, int flag)
{
    int newCord;
    rand()%2 ? (newCord= coordinate-1) : (newCord = coordinate+1);
    if (flag==0)  // N-coordinate
    {
        if (newCord<0)
            return N-1;
        else if (newCord>N-1)
            return 0;
    }
    else if (flag==1)  // M-coordinate
    {
        if (newCord<0)
            return M-1;
        else if (newCord>M-1)
            return 0;
    }
    return newCord;
}
