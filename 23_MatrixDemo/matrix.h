#ifndef MATRIX_H
#define MATRIX_H
#include <vector>
#include <iostream>

using std::ostream;
class Matrix
{
    typedef std::vector< std::vector<double> > MyMatrix;
    int N;
    int M;
    MyMatrix mat;

public:
    Matrix() : N(0), M(0) {}
    Matrix(int n, int m);

    Matrix operator+(Matrix & m);
    Matrix operator-(Matrix & m);

    Matrix operator*(int i);
    Matrix operator*(Matrix & m);

    std::vector<double> & operator[](int i) { return mat[i]; }

    bool operator==(Matrix & m);
    bool operator!=(Matrix & m) { return !((*this) == m); }

    void fill(void); //exeption = const char *
    void random_fill(void);
    void show(void);

    Matrix getRevers(void); //exeption = const char *
    friend Matrix matForRevers(Matrix & m, int x, int y);

    Matrix trans(void);

    //Friends
    friend ostream & operator<<(ostream & os, Matrix & m) { m.show(); return os; }
    friend Matrix operator*(int i, Matrix m) { return m * i; }

    friend double det(Matrix & m);
    friend Matrix setTemp(Matrix & m, int x);

};

#endif // MATRIX_H