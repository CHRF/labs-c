#include "DateTime.h"
#include <ctime>
#include <iostream>

using namespace std;

int main()
{
	DateTime date(17,6,2014);
	cout << "program written:\t";
	date.printToday();

	DateTime date2;
	cout << "today:\t\t";
	date2.printToday();
	cout << "year future:\t";
	date2.printFuture(365);
	cout << "tomorrow:\t";
	date2.printTomorrow();

	cout << "year past:\t";
	date2.printPast(365);
	cout << "yesterday:\t";
	date2.printYesterday();


	cout << "difference (date1 and date2): ";
	date.printDiff(date2);

	return 0;
}
