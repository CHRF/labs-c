#include "DateTime.h"
#include <ctime>
#include <iostream>

using namespace std;


DateTime::DateTime()
{
	time_t curTime = time(NULL);
	struct tm * sysTime = localtime(&curTime);
	day = sysTime->tm_mday;
	mon = (sysTime->tm_mon)+1;
	yr  = (sysTime->tm_year)+1900;
}

DateTime::DateTime(int _day, int _mon, int _yr)
{
	yr  = _yr;
        mon = _mon;
        day = _day;

}

DateTime::DateTime(const DateTime & _DateTime)
{
	day = _DateTime.day;
	mon = _DateTime.mon;
	yr =  _DateTime.yr;
}


int DateTime::getWeekDay()
{
	int a = (14 - mon) / 12;
	int y = yr - a;
	int m = mon + 12 * a - 2;
	return (7000 + (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12)) % 7;
}


const char * DateTime::getWeekDayText()
{
	switch (getWeekDay())
	{
	case 1: return "Monday";		break;
	case 2: return "Tuesday";		break;
	case 3: return "Wednesday";		break;
	case 4: return "Thursday";		break;
	case 5: return "Friday";		break;
	case 6: return "Saturday";		break;
	case 0: return "Sunday";		break;
	default: return NULL;
	}
}

const char * DateTime::getMonthText()
{
	switch (mon)
	{
	case 1:  return "January";		break;
	case 2:  return "Febrary";		break;
	case 3:  return "March";		break;
	case 4:  return "April";		break;
	case 5:  return "May";			break;
	case 6:  return "June";			break;
	case 7:  return "July";			break;
	case 8:  return "August";		break;
	case 9:  return "September";    	break;
	case 10: return "October";		break;
	case 11: return "November";		break;
	case 12: return "December";		break;
	default: return NULL;
	}
}


void DateTime::printToday()
{
	cout << getWeekDayText() << ", " << day << ", " << getMonthText() << ", " << yr << endl;
}

void DateTime::printMonth()
{
	cout << getMonthText() << endl;
}

void DateTime::printWeekDay()
{
	cout << getWeekDayText() << endl;
}

DateTime DateTime::getFuture(const int diff)const
{
	int _day=day, _mon=mon, _yr=yr;
	for (int i = 0; i < diff; ++i)
	{		
		_day++;
		if (_day<29) continue;
		else if ((_mon == 1 || _mon == 3 || _mon == 5 || _mon == 7 || _mon == 8 || _mon == 10 || _mon == 12) && _day>31) { _day = 1; _mon++; }
		else if ((_mon == 4 || _mon == 6 || _mon == 9 || _mon == 11) && _day>30) { _day = 1; _mon++; }
		else if (_mon == 2 && (_yr % 400 == 0 || _yr % 4 == 0) && _day > 29) { _day = 1; _mon++; }
		else if (_mon == 2 && (_yr % 100 == 0 || _yr % 4 != 0) && _day > 28) { _day = 1; _mon++; }
		if (_mon > 12) {_mon = 1;  _yr++;}

	}

	return DateTime(_day, _mon, _yr);
}

DateTime DateTime::getPast(const int diff)const
{
	int _day = day, _mon = mon, _yr = yr;
	for (int i = 0; i < diff; ++i)
	{
		_day--;
		if (_day>0) continue;
		else 
		{
			_mon--;
			if (_mon < 1) { _mon = 12;  _yr--; }
			if (_mon == 1 || _mon == 3 || _mon == 5 || _mon == 7 || _mon == 8 || _mon == 10 || _mon == 12) _day = 31;
			else if (_mon == 4 || _mon == 6 || _mon == 9 || _mon == 11) _day = 30;
			else if (_mon == 2 && (_yr % 400 == 0 || _yr % 4 == 0)) _day = 29;
			else if (_mon == 2 && (_yr % 100 == 0 || _yr % 4 != 0)) _day = 28;

		}
	}

	return DateTime(_day, _mon, _yr);
}

void DateTime::printPast(const int diff)const
{
	getPast(diff).printToday();
}

void DateTime::printPast()const
{
	getPast(1).printToday();
}

void DateTime::printYesterday()const
{
	getPast(1).printToday();
}

void DateTime::printFuture(const int diff)const
{
	getFuture(diff).printToday();
}

void DateTime::printFuture()const
{
	getFuture(1).printToday();
}

void DateTime::printTomorrow()const
{
	getFuture(1).printToday();
}


int DateTime::getDiff(const DateTime & _DateTime)const
{
	int i = 0;
	if (yr < _DateTime.yr || mon < _DateTime.mon || day < _DateTime.day)
	{
		int _day = day, _mon = mon, _yr = yr;
		while ( _yr < _DateTime.yr || _mon < _DateTime.mon || _day < _DateTime.day)
		{	
			i++;
			_day++;
			if (_day<29) continue;
			else if ((_mon == 1 || _mon == 3 || _mon == 5 || _mon == 7 || _mon == 8 || _mon == 10 || _mon == 12) && _day>31) { _day = 1; _mon++; }
			else if ((_mon == 4 || _mon == 6 || _mon == 9 || _mon == 11) && _day>30) { _day = 1; _mon++; }
			else if (_mon == 2 && (_yr % 400 == 0 || _yr % 4 == 0) && _day > 29) { _day = 1; _mon++; }
			else if (_mon == 2 && (_yr % 100 == 0 || _yr % 4 != 0) && _day > 28) { _day = 1; _mon++; }
			if (_mon > 12) { _mon = 1;  _yr++; }
			
		}
		return i;
	}
	else if (yr > _DateTime.yr || mon > _DateTime.mon || day > _DateTime.day)
	{
		int _day = day, _mon = mon, _yr = yr;
		while (_yr > _DateTime.yr || _mon > _DateTime.mon || _day > _DateTime.day)
		{
			i++;
			_day--;
			if (_day>0) continue;
			else
			{
				_mon--;
				if (_mon < 1) { _mon = 12;  _yr--; }
				if (_mon == 1 || _mon == 3 || _mon == 5 || _mon == 7 || _mon == 8 || _mon == 10 || _mon == 12) _day = 31;
				else if (_mon == 4 || _mon == 6 || _mon == 9 || _mon == 11) _day = 30;
				else if (_mon == 2 && (_yr % 400 == 0 || _yr % 4 == 0)) _day = 29;
				else if (_mon == 2 && (_yr % 100 == 0 || _yr % 4 != 0)) _day = 28;
				
			}
			
		}
		return i;
	}
	else return 88;
}


void DateTime::printDiff(const DateTime & _DateTime)const
{
	cout << getDiff(_DateTime) << endl;
}
