#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_finishDT_editingFinished();
    void on_startDT_editingFinished();
    void calcDiffMax();
    void calcProgress();

private:
    int diff;
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H