#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    calcDiffMax();
    calcProgress();

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(calcProgress()));
    timer->start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_finishDT_editingFinished()
{
    calcDiffMax();
    calcProgress();
}

void MainWindow::on_startDT_editingFinished()
{
    calcDiffMax();
    calcProgress();
}

void MainWindow::calcDiffMax()
{

    diff=ui->startDT->dateTime().secsTo(ui->finishDT->dateTime());
    if(diff>0) ui->progressBar->setMaximum(diff);

}

void MainWindow::calcProgress()
{
    if(diff>0 && ui->finishDT->dateTime() > QDateTime::currentDateTime() )
    {
        if(ui->startDT->dateTime() < QDateTime::currentDateTime())
        ui->progressBar->setValue(ui->startDT->dateTime().secsTo(QDateTime::currentDateTime()));
        else ui->progressBar->setValue(0);
    }
    else if (diff<0 ) {ui->progressBar->setValue(0);}
    else {ui->progressBar->setMaximum(100); ui->progressBar->setValue(100);}
}